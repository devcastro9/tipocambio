﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TipoCambio.Models;

[Table("gc_tipo_cambio")]
public partial class GcTipoCambio
{
    [Key]
    [Column("fecha_cambio", TypeName = "date")]
    public DateTime FechaCambio { get; set; }

    [Column("cambio_oficial_compra", TypeName = "decimal(18, 3)")]
    public decimal? CambioOficialCompra { get; set; }

    [Column("cambio_mercado_venta", TypeName = "decimal(18, 3)")]
    public decimal? CambioMercadoVenta { get; set; }

    [Column("cambio_ufv", TypeName = "decimal(18, 5)")]
    public decimal? CambioUfv { get; set; }

    [Column("cambio_eur", TypeName = "decimal(18, 3)")]
    public decimal? CambioEur { get; set; }

    [Column("cambio_rmb", TypeName = "decimal(18, 3)")]
    public decimal? CambioRmb { get; set; }

    [Column("cambio_brl", TypeName = "decimal(18, 3)")]
    public decimal? CambioBrl { get; set; }

    [Column("usr_codigo")]
    [StringLength(20)]
    [Unicode(false)]
    public string? UsrCodigo { get; set; }

    [Column("fecha_registro", TypeName = "date")]
    public DateTime? FechaRegistro { get; set; }

    [Column("hora_registro")]
    [StringLength(8)]
    [Unicode(false)]
    public string? HoraRegistro { get; set; }
}
