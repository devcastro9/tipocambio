using System;
using System.Globalization;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using TipoCambio.Utility;
using TipoCambio.ViewModels;

namespace TipoCambio.Services;

public class ApiLayer
{
    private readonly HttpClient _httpClient;
    public ApiLayer(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<ApiLayerRes?> ConvertValue(string MonedaTo, string MonedaFrom, Decimal Monto, DateTime Fecha, string ApiKey)
    {
        string FechaVal = Fecha.ToString("yyyy-MM-dd");
        string MontoVal = Monto.ToString("0.00", CultureInfo.InvariantCulture);
        _httpClient.DefaultRequestHeaders.Add("apikey", ApiKey);
        using HttpResponseMessage response = await _httpClient.GetAsync($"https://api.apilayer.com/exchangerates_data/convert?to={MonedaTo}&from={MonedaFrom}&amount={MontoVal}&date={FechaVal}");
        response.EnsureSuccessStatusCode();
        // Deserializacion de la respuesta
        JsonSerializerOptions options = new()
        {
            ReferenceHandler = ReferenceHandler.IgnoreCycles
        };
        options.Converters.Add(new FormatDateTimeConverter("yyyy-MM-dd"));
        string ContentResponse = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<ApiLayerRes>(ContentResponse, options);
    }
}