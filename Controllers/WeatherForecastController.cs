using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TipoCambio.Services;
using TipoCambio.ViewModels;

namespace TipoCambio.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly BcbApi _bcbApi;
    private readonly ApiLayer _apiLayer;
    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(BcbApi bcbApi, ApiLayer apiLayer, ILogger<WeatherForecastController> logger)
    {
        _bcbApi = bcbApi;
        _apiLayer = apiLayer;
        _logger = logger;
    }

    [HttpGet("GetWeatherForecast")]
    public IEnumerable<WeatherForecast> Get()
    {
        return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        {
            Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            TemperatureC = Random.Shared.Next(-20, 55),
            Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        })
        .ToArray();
    }

    [HttpGet("GetUfv")]
    public async Task<IList<BcbUfvRes>?> GetValores()
    {
        return await _bcbApi.GetUfv(new DateTime(2023, 04, 01), new DateTime(2023, 06, 10));
    }

    [HttpGet("GetEur")]
    public async Task<ApiLayerRes?> GetEuro()
    {
        string api = "7Y1XipzWFVoyVDDGvFk3yuh4W7cFhAuq";
        return await _apiLayer.ConvertValue("BOB", "EUR", 0.99m, new DateTime(2023, 5, 13), api);
    }
}
