﻿using Microsoft.EntityFrameworkCore;
using TipoCambio.Models;

namespace TipoCambio.Data;

public partial class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<GcTipoCambio> GcTipoCambios { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<GcTipoCambio>(entity =>
        {
            entity.Property(e => e.CambioBrl).HasDefaultValueSql("((1))");
            entity.Property(e => e.CambioEur).HasDefaultValueSql("((8))");
            entity.Property(e => e.CambioRmb).HasDefaultValueSql("((1))");
            entity.Property(e => e.FechaRegistro).HasDefaultValueSql("(getdate())");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
