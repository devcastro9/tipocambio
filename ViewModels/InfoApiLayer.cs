using System.Text.Json.Serialization;

namespace TipoCambio.ViewModels;

public class InfoApiLayer
{
    [JsonPropertyName("timestamp")]
    public long Tiempo { get; set; }

    [JsonPropertyName("rate")]
    public decimal Tasa { get; set; }
}
